package core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GitRepository {
	String repoPath;
	
	public GitRepository(String repoPath) {
		this.repoPath = repoPath;
	}
	
	public String getHeadRef() throws FileNotFoundException {
		Scanner completePath = getCompletePath("HEAD");
		String ref = completePath.nextLine().split(" ")[1];
		completePath.close();
		return ref;
	}

	public String getRefHash(String ref) throws FileNotFoundException {
		Scanner completePath = getCompletePath(ref);
		String hash = completePath.nextLine();
		completePath.close();
		return hash;
	}

	private Scanner getCompletePath(String ref) throws FileNotFoundException {
		return new Scanner(new FileInputStream(repoPath + "/" + ref));
	}
}